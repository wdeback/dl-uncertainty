import numpy as np

################################################

def generate_segmentation_opening(im, disk_size=1, min_size=150):
    '''
    
    '''
    from skimage.morphology import opening, disk
    im_opening = opening(im.astype(np.int), disk(disk_size))

    # remove small remaining objects 
    from skimage.morphology import remove_small_objects
    im_opening_clean = remove_small_objects(im_opening.astype(np.bool), min_size=min_size, connectivity=2, in_place=False)

    return im_opening_clean
   
################################################    
    
def generate_segmentation(im, remove_fraction, min_size=150):
    '''
    
    Args:
    - im (ndarray, int): image with original segmentation
    - remove_fraction (float, [0.,1.]): fraction narrowest branches to remove
    - min_size (int): remove objects below this size (after removing branches)
    
    Returns:
    - (ndarray, shape=im.shape)image with bracnhes removed 
    '''
    
    im = im.astype(np.int)

    # skeletonize image
    from skimage.morphology import skeletonize, medial_axis
    im_skel = skeletonize(im)

    # count neighbors
    from scipy.ndimage.filters import convolve
    kernel1 = np.array([[0,1,0],
                        [1,0,1],
                        [0,1,0]])
    im_nbs = convolve(im_skel.astype(np.int), kernel1, mode='constant')
    kernel2 = np.array([[1,0,1],
                        [0,0,0],
                        [1,0,1]])
    im_nbs += convolve(im_skel.astype(np.int), kernel2, mode='constant')
    im_nbs *= im_skel
    
    # get the points with 3 or more neighbors
    im_branch_points = np.zeros_like(im_nbs)
    im_branch_points[ im_nbs >= 3 ] = 1

    # get local width of structure
    from scipy.ndimage.morphology import distance_transform_edt
    im_widths = distance_transform_edt(im)

    # draw branching points with diameter = width of structure
    from skimage.draw import circle
    bps = np.array(np.where(im_branch_points == 1)).T
    im_bps = np.zeros_like(im)
    for bp in bps:
        x, y = bp
        radius = im_widths[x,y] * 2.0 + 0.1
        r,c = circle(x, y, radius)
        im_bps[r,c] = 1

    
    # label individual branches
    from skimage.morphology import label

    # subtract branching points from structure 
    # to isolate individual branches
    im_branches = np.clip(im-im_bps, a_min=0, a_max=1)

    im_label = label(im_branches, neighbors=8)

    # gather width/length of each branch
    branch_labels = np.unique(im_label)[1:]

    widths, lengths = [], []
    for label in branch_labels:

        coords_branch = np.array(np.where(im_label == label)).T
        x, y  = coords_branch[:,0], coords_branch[:,1]

        # get length of branch: number of point on skeleton 
        length = np.sum(im_skel[x,y])

        # get average width: width at skeleton points, divided by length
        width = np.sum(2.0*im_widths[x,y] * im_skel[x,y]) / (length+1.0)

        #print('branch {: >3d}: length = {: >5d}, width = {: >6.04f}'.format(label, length, width))

        branch = {}
        widths.append( width )
        lengths.append( length )
        
    
    # sort branches according to their width
    widths = np.array(widths)
    ind = np.argsort( widths )
    # get narrowest 65% 
    narrow_branches = branch_labels[ind][:int(len(widths) * remove_fraction)]
    
    
    # remove narrowest branches 
    im_removed = im.copy()
    for narrow_branch in narrow_branches:
        im_removed[ im_label == narrow_branch ] = 0

    # remove small remaining objects 
    from skimage.morphology import remove_small_objects
    im_removed = im_removed.astype(np.bool)
    im_removed_clean = remove_small_objects(im_removed, min_size=min_size, connectivity=2, in_place=False)

    return im_removed_clean

########################################